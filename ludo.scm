;; ludo.scm --- ludo implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2019 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-ludo.
;;
;; guile-ludo is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-ludo is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-ludo; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (ludo)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (ludo dice)
  #:re-export (sdie
               ndie d1 d2 d4 d6 d8 d10 d12 d20 d100

               combine roll xdx))

;; Cards

(define (deck cards)
  "Return a die which, when rolled, returns 2 values: the face of the die and
a new die with one fewer side."
  (apply die (map (lambda (v)
                    (lambda (s)
                      (values v (deck (delete v cards)))))
                  cards)))

(define (draw cards deck)
  "Return 2 values, a list of drawn cards and a deck, the remainder of the
cards."
  (let lp ((count cards)
           (cards deck)
           (result '()))
    (if (zero? count)
        (values result cards)
        (call-with-values
            (lambda _ (roll cards))
          (match-lambda*
            (((? eod? e)) (throw 'empty-deck result cards))
            ((value deck)
             (lp (1- count) deck (cons value result))))))))

(define* (deal hands deck #:optional (cards 1))
  "Return 2 values, a list of hands with cards from DECK and a new deck, with
the remainder of cards."
  (let lp ((hands-left hands)
           (result '())
           (deck deck))
    (if (zero? hands-left)
        (values result deck)
        (catch 'empty-deck
          (lambda _
            (call-with-values
                (lambda _ (draw cards deck))
              (match-lambda*
                ((v d)
                 (lp (1- hands-left)
                     (cons v result)
                     d)))))
          (lambda (key cards deck)
             (throw 'empty-deck (cons cards result)))))))
