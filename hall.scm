(hall-description
  (name "ludo")
  (prefix "guile")
  (version "0.1")
  (author "Alex Sassmannshausen")
  (copyright (2019))
  (synopsis "A dice and card library")
  (description
    "Guile Ludo is a high-level playing card and dice library.  It provides generators, and utility functions.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/")
  (license gpl3+)
  (dependencies dependencies)
  (skip ())
  (files (libraries
           ((scheme-file "ludo") (directory "ludo" ())))
         (tests ((directory "tests" ())))
         (programs ((directory "scripts" ())))
         (documentation
           ((text-file "ChangeLog")
            (text-file "AUTHORS")
            (text-file "NEWS")
            (text-file "README")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "ludo")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))
