;; ludo/dice.scm --- dice implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2021 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-ludo.
;;
;; guile-ludo is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-ludo is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-ludo; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (ludo dice)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (oop goops)
  #:export (<side>
            <string-side> <numeric-side>

            <die>
            <string-die> <numeric-die>
            d1 d2 d4 d6 d8 d10 d12 d20 d100
            ndie sdie

            <pool>

            combine roll

            xdx))

;;;; Classes

;;;;; Sides

(define-class <side> ()
  (value #:init-keyword #:value #:accessor value))

(define-class <numeric-side> (<side>))

(define-method (initialize (n <numeric-side>) initargs)
  (next-method)
  (when (not (number? (value n)))
    (throw 'numeric-side "Value must be numeric." n))
  n)

(define-class <string-side> (<side>))

(define-method (initialize (n <string-side>) initargs)
  (next-method)
  (when (not (string? (value n)))
    (throw 'string-side "Value must be a string." n))
  n)

;;;;; Dice

(define-class <die> ()
  (sides #:init-keyword #:sides #:accessor sides #:init-form '()))

(define-class <numeric-die> (<die>))

(define-method (initialize (n <numeric-die>) initargs)
  (next-method)
  (match (sides n)
    (((? (cute is-a? <> <numeric-side>)) ...) n)
    (_ (throw 'numeric-die "Sides must be numeric." n))))

(define-class <string-die> (<die>))

(define-method (initialize (n <string-die>) initargs)
  (next-method)
  (match (sides n)
    (((? (cute is-a? <> <string-side>)) ...) n)
    (_ (throw 'string-die "Sides must be a string." n))))

;;;;; Pools

(define-class <pool> ()
  (dice #:init-keyword #:dice #:accessor dice #:init-form '()))

;;;; Methods

;;;;; Combine

(define-method (combine . n)
  (match n
    (((? (cute is-a? <> <numeric-side>)) ...)
     (make <numeric-die> #:sides n))
    (((? (cute is-a? <> <string-side>)) ...)
     (make <string-die> #:sides n))
    (((? (cute is-a? <> <side>)) ...)
     (make <die> #:sides n))
    (((? (cute is-a? <> <die>)) ...)
     (make <pool> #:dice n))
    (((? (cute is-a? <> <pool>)) ...)
     (make <pool> #:dice (append (map dice n))))
    (_ (throw 'combination-impossible n))))

(define-method (combine (d <die>) . n)
  (match n
    (((? (cute is-a? <> <side>)) ...)
     (apply combine (append (sides d) n)))
    (_ (next-method))))

(define-method (combine (p <pool>) . n)
  (match n
    (((? (cute is-a? <> <die>)) ...)
     (apply combine (append (dice p) n)))
    (_ (next-method))))

;;;;; Roll

(define-method (roll (s <side>))
  (value s))

(define-method (roll (d <die>))
  (roll (list-ref (sides d) (random (length (sides d))))))

(define-method (roll (p <pool>))
  (map roll (dice p)))

;;;; Convenience

(define-method (xdx (i <integer>) (d <die>))
  (apply combine (map (λ (_) d) (iota i))))

;;;;; Numeric Dice

(define-method (ndie (n <number>))
  (apply combine (map (compose (cute make <numeric-side> #:value <>)
                               1+)
                      (iota n))))

(define-syntax-rule (die-name n)
  (datum->syntax
   n (string->symbol
      (string-append "d" (number->string (syntax->datum n))))))

(define-syntax dndie
  (lambda (x)
    (syntax-case x ()
      ((_ die)
       #`(define #,(die-name #'die)
           (ndie die))))))

(dndie 1)
(dndie 2)
(dndie 3)
(dndie 4)
(dndie 6)
(dndie 8)
(dndie 10)
(dndie 12)
(dndie 20)
(dndie 100)

;;;;; String Dice

(define-method (sdie . str)
  (apply combine (map (cute make <string-side> #:value <>) str)))
