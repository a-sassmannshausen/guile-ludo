(use-modules
  (guix packages)
  (guix licenses)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "guile-ludo")
  (version "0.1")
  (source "./guile-ludo-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-2.2)))
  (propagated-inputs `())
  (synopsis "A dice and card library")
  (description
    "Guile Ludo is a high-level playing card and dice library.  It provides generators, and utility functions.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/")
  (license gpl3+))

